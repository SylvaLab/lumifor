# -*- coding: utf-8 -*-
"""
Software: LUMIFOR
File: calc_duration.pyx
Copyright (C) Sylvain DUPIRE - SylvaLab 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@sylvalab.fr
Version: 1.0
Date: 2021/08/13
License :  GNU-GPL V3
"""
import numpy as np
cimport numpy as np
import cython
cimport cython

##############################################################################################################################################
### Fonctions generales
##############################################################################################################################################
cdef extern from "math.h":
    double acos(double)
    double atan(double)
    double asin(double)
    double sqrt(double)
    double cos(double)
    double fabs(double)
    double log(double)
    double cos(double)
    double tan(double)
    double sin(double) 
    double sinh(double)
    double cosh(double)
    int floor(double)
    int ceil(double)    
    double atan2(double a, double b)
    double degrees(double)
    bint isnan(double x)


cdef extern from "numpy/npy_math.h":
    bint npy_isnan(double x)

cdef extern from "Python.h":
    Py_ssize_t PyList_Size(object) except -1   #Equivalent de len(list)
    int PyList_Append(object, object) except -1 #Equivalent de list.append()
    object PyList_GetSlice(object, Py_ssize_t low, Py_ssize_t high) #Equivalent de list[low:high]
    int PyList_Sort(object) except -1 #Equivalent de list.sort()

ctypedef np.int_t dtype_t
ctypedef np.float_t dtypef_t
ctypedef np.int8_t dtype8_t
ctypedef np.uint8_t dtypeu8_t
ctypedef np.uint16_t dtypeu16_t
ctypedef np.int16_t dtype16_t
ctypedef np.int32_t dtype32_t
ctypedef np.int64_t dtype64_t
ctypedef np.float32_t dtypef32_t

# Retourne le maximum d'un array
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef int max_array(np.ndarray[dtype32_t, ndim=1] a):
    cdef int max_a = a[0]
    cdef unsigned int item = 1
    for item from 1 <= item < a.shape[0]:
        if a[item] > max_a:max_a = a[item]
    return max_a

# Retourne le maximum d'un array de float
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double max_array_f(np.ndarray[dtypef_t, ndim=1] a):
    cdef double max_a = a[0]
    cdef unsigned int item = 1
    for item from 1 <= item < a.shape[0]:
        if a[item] > max_a:max_a = a[item]
    return max_a

# Retourne la somme d'un array
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double sum_array_f(np.ndarray[dtypef_t, ndim=1] a):
    cdef double summ = a[0]
    cdef unsigned int item = 1
    for item from 1 <= item < a.shape[0]:
        summ += item
    return summ

@cython.boundscheck(False)
@cython.wraparound(False)
cdef int sum_array_uint16(np.ndarray[dtypeu16_t, ndim=1] a):
    cdef int summ = a[0]
    cdef unsigned int item = 1
    for item from 1 <= item < a.shape[0]:
        summ += item
    return summ

# Definie la fonction arcsinus hyperbolique
cdef inline double asinh(double x):return log(x+sqrt(1+x*x))
cdef inline int int_max(int a, int b): return a if a >= b else b
cdef inline int int_min(int a, int b): return a if a <= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b
cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double square(double x):return x*x
cdef inline int cint(double x):return int(x)
#cdef inline int isnan(value):return 1 if value != value else 0


cdef double pi=3.141592653589793

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double modulo(double a,double b):
    cdef double val,ent
    if a<b and a>=0:
        val=a
    elif a<b and a<0:
        ent = cint(a/b)
        val = b+(a-ent*b)
    else:
        ent = cint(a/b)
        val = a-ent*b
    return val

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef float calculate_azimut(float x1,float y1,float x2,float y2):   
    cdef float DX = x2-x1
    cdef float DY = y2-y1
    cdef float Deuc = sqrt(DX*DX+DY*DY)
    cdef int Fact=-1
    cdef float Angle
    if x2>x1:Fact=1
    if Deuc==0:
        Angle=0
    else:
        Angle = acos(DY/Deuc)*180/pi
        Angle *=Fact
    return modulo(Angle,360)

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double conv_az_to_polar(double az):
    cdef double val
    val = (360-(az-90))
    return modulo(val,360)

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double Distplan(double x, double y, double xE, double yE):
    return sqrt((y-yE)*(y-yE)+(x-xE)*(x-xE))

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef (int, double, double) intersection_2segments(double x1, double y1, double x2, double y2,
                             double x3, double y3, double x4, double y4):
    """
    1 et 2 : point du segment 1
    3 et 4 : point du segment 2
    """
    cdef double denom = (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)
    cdef double x=-1,y=-1
    cdef int test=0    
    cdef double xmin = double_max(double_min(x1,x2),double_min(x3,x4))
    cdef double xmax = double_min(double_max(x1,x2),double_max(x3,x4))
    cdef double ymin = double_max(double_min(y1,y2),double_min(y3,y4))
    cdef double ymax = double_min(double_max(y1,y2),double_max(y3,y4))
    if denom!=0:
        test=1
        x = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/denom
        y = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/denom
        
        if x < xmin and fabs(x-xmin)>0.01:
            test = 0
        elif x > xmax and fabs(x-xmax)>0.01:
            test = 0
        if y < ymin and fabs(y-ymin)>0.01:
            test = 0
        elif y > ymax and fabs(y-ymax)>0.01:
            test = 0           
    return test,x,y

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtypef_t,ndim=2] get_az_vertices(np.ndarray[dtypef_t,ndim=2] vertices):
    cdef int nbvertice = vertices.shape[0]
    cdef np.ndarray[dtypef_t,ndim=2] segments = np.zeros((nbvertice-1,7),dtype=np.float)
    cdef int i=0 
    for i in range(0,nbvertice-1):
        segments[i,0],segments[i,1] = vertices[i,0],vertices[i,1]
        segments[i,2],segments[i,3] = vertices[i+1,0],vertices[i+1,1]  
    return segments

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef int selec_seg(double az, np.ndarray[dtypef_t,ndim=2] segments):
    cdef int nbseg = segments.shape[0]
    cdef int line = -1
    cdef int lineN = -1
    cdef int i=0 
    for i in range(0,nbseg):
        if not segments[i,6]:
            if az>=segments[i,4] and az<=segments[i,5]:
                line=i
                break
        else:
            lineN = i
            continue
    if line<0:
        line = lineN
    return line

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef float get_elev(np.ndarray[dtypef32_t,ndim=2] Mns, 
                    float x,float y):    
    #check if x,y is integer
    cdef int xi = cint(x)
    cdef int yi = cint(y)
    cdef int xok=0
    cdef int yok=0
    cdef float alt,sd,d
    if xi==x:
        xok = 1
    if yi==y:
        yok = 1
    # config 1 : Xint Yint
    if xok and yok:
        alt = Mns[yi,xi]
    # config 2 : Xint Y
    elif xok and not yok:
        d = 1/fabs(yi-y)
        alt = Mns[yi,xi]*d        
        sd = d
        d = 1/fabs(yi+1-y)
        alt = alt + Mns[yi+1,xi]*d        
        sd = sd+d
        alt = alt/sd      
    # config 2 : X Yint
    elif not xok and yok:  
        d = 1/fabs(xi-x)
        alt = Mns[yi,xi]*d
        sd = d
        d = 1/fabs(xi+1-x)
        alt = alt + Mns[yi,xi+1]*d      
        sd = sd+d
        alt = alt/sd
    else:
        d = 1/sqrt((xi-x)*(xi-x)+(yi-y)*(yi-y))
        alt = Mns[yi,xi]*d
        sd = d
        d = 1/sqrt((xi+1-x)*(xi+1-x)+(yi-y)*(yi-y))
        alt = alt + Mns[yi,xi+1]*d       
        sd = sd+d
        d = 1/sqrt((xi+1-x)*(xi+1-x)+(yi+1-y)*(yi+1-y))
        alt = alt + Mns[yi+1,xi+1]*d
        sd = sd+d
        d = 1/sqrt((xi-x)*(xi-x)+(yi+1-y)*(yi+1-y))
        alt = alt + Mns[yi+1,xi]*d
        sd = sd+d
        
        alt = alt/sd
     
    return alt

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtype32_t,ndim=2] sun_duration_virt_plane(np.ndarray[dtype32_t,ndim=2] hole_list,
                                                           np.ndarray[dtypef_t,ndim=2] vertices,
                                                           np.ndarray[dtypef32_t,ndim=2] Dtm,                                                            
                                                           np.ndarray[dtype32_t,ndim=2] Sun_posi,
                                                           np.ndarray[dtype32_t,ndim=2] Az_val,
                                                           double dim1, double dim2,float Stand_H,
                                                           double Csize):
    cdef int nbhole = hole_list.shape[0]
    cdef int jmax = Az_val.shape[0]-1
    cdef int azmin = Az_val[0,0]
    cdef int azmax = Az_val[jmax,0]
    cdef int h,i,line,test,yo,xo,j ,mi,ma ,az
    cdef double yf,xf,y3,x3,y4,x4,x,y,sl,dist,pol    
    cdef float zo,z
    cdef int nbval = Sun_posi.shape[0]
    cdef int nbsegment = vertices.shape[0]-1
    cdef double radius = (sqrt(dim1*dim1+dim2*dim2)+double_max(dim1,dim2))/Csize
    cdef np.ndarray[dtype32_t,ndim=2] Sun_duration = np.ones_like(Dtm,dtype=np.int32)*-1  
    cdef np.ndarray[dtypef32_t,ndim=2] Dtm2 = Dtm+Stand_H
    cdef np.ndarray[dtypef_t,ndim=2] segments = get_az_vertices(vertices)


    for h in range(0,nbhole):       
        yo,xo=hole_list[h,0],hole_list[h,1]
        zo=Dtm[yo,xo]      
        Sun_duration[yo,xo]=0 
        #calculation of azimuth of each segment
        for i in range(0,nbsegment):
            segments[i,4]=calculate_azimut(xo,-yo,segments[i,0],-segments[i,1])
            segments[i,5]=calculate_azimut(xo,-yo,segments[i,2],-segments[i,3])
            if segments[i,4]>segments[i,5]:
                segments[i,6]=1
            else:
                segments[i,6]=0
        
        for j in range(0,jmax+1):
            az = Az_val[j,0]
            pol = conv_az_to_polar(az)*pi/180            
            xf = xo+radius*cos(pol)
            yf = yo-radius*sin(pol)
            line = selec_seg(az,segments)
            x3,y3,x4,y4=segments[line,0],segments[line,1],segments[line,2],segments[line,3]
            test,x,y = intersection_2segments(xo, yo, xf, yf,
                                              x3, y3, x4, y4)
            if test:
                mi=Az_val[j,1]                 
                if j<jmax:
                    ma = Az_val[j+1,1]
                else:
                    ma = nbval  
                dist = Distplan(xo,yo,x,y)*Csize+0.00000000001
                z = get_elev(Dtm2,x,y) 
                sl = (z-zo)/dist
                if Sun_posi[ma-1,1]/10000.>sl:                                           
                    Sun_duration[yo,xo]=Sun_duration[yo,xo]+Sun_posi[ma-1,2]
                    for line in range(ma-2,mi-1,-1):
                        if Sun_posi[line,1]/10000.>sl:
                            Sun_duration[yo,xo]=Sun_duration[yo,xo]+Sun_posi[line,2]
                        else:
                            break   
    return Sun_duration

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtype32_t,ndim=2] sun_duration_virt_plane21(np.ndarray[dtype32_t,ndim=2] hole_list,
                                                             np.ndarray[dtypef_t,ndim=2] vertices,
                                                             np.ndarray[dtypef32_t,ndim=2] Dtm, 
                                                             np.ndarray[dtype32_t,ndim=2] Sun_posi,
                                                             np.ndarray[dtype32_t,ndim=2] Az_val,
                                                             double dim1, double dim2,float Stand_H,
                                                             double Csize):
    
    cdef int nbhole = hole_list.shape[0]
    cdef int jmax = Az_val.shape[0]-1
    cdef int azmin = Az_val[0,0]
    cdef int azmax = Az_val[jmax,0]
    cdef int h,i,line,test,yo,xo,j ,mi,ma ,az,m
    cdef double yf,xf,y3,x3,y4,x4,x,y,sl,dist,pol    
    cdef float zo,z
    cdef int nbval = Sun_posi.shape[0]
    cdef int nbsegment = vertices.shape[0]-1
    cdef double radius = (sqrt(dim1*dim1+dim2*dim2)+double_max(dim1,dim2))/Csize
    cdef int nrows = Dtm.shape[0]
    cdef int ncols =  Dtm.shape[1]
    cdef np.ndarray[dtype32_t,ndim=3] Sun_duration = np.ones((12,nrows,ncols),dtype=np.int32)*-1
    cdef np.ndarray[dtypef32_t,ndim=2] Dtm2 = Dtm+Stand_H
    cdef np.ndarray[dtypef_t,ndim=2] segments = get_az_vertices(vertices)
    
    
    #loop on hole pix:
    for h in range(0,nbhole):
        yo,xo=hole_list[h,0],hole_list[h,1]
        zo= Dtm[yo,xo] 
        for m in range(0,12):
            Sun_duration[m,yo,xo]=0 
        
        #calculation of azimuth of each segment
        for i in range(0,nbsegment):
            segments[i,4]=calculate_azimut(xo,-yo,segments[i,0],-segments[i,1])
            segments[i,5]=calculate_azimut(xo,-yo,segments[i,2],-segments[i,3])
            if segments[i,4]>segments[i,5]:
                segments[i,6]=1
            else:
                segments[i,6]=0
        
        #test of each azimut
        for j in range(0,jmax+1):           
            az = Az_val[j,0]
            pol = conv_az_to_polar(az)*pi/180            
            xf = xo+radius*cos(pol)
            yf = yo-radius*sin(pol)
            line = selec_seg(az,segments)
            x3,y3,x4,y4=segments[line,0],segments[line,1],segments[line,2],segments[line,3]
            test,x,y = intersection_2segments(xo, yo, xf, yf,
                                              x3, y3, x4, y4)
            if test:
                mi=Az_val[j,1]                 
                if j<jmax:
                    ma = Az_val[j+1,1]
                else:
                    ma = nbval  
                dist = Distplan(xo,yo,x,y)*Csize+0.00000000001
                z = get_elev(Dtm2,x,y) 
                sl = (z-zo)/dist
                if Sun_posi[ma-1,1]/10000.>sl:                 
                    for m in range(0,12):    
                        Sun_duration[m,yo,xo]=Sun_duration[m,yo,xo]+Sun_posi[ma-1,2+m]
                    for line in range(ma-2,mi-1,-1):
                        if Sun_posi[line,1]/10000.>sl:
                            for m in range(0,12):   
                                Sun_duration[m,yo,xo]=Sun_duration[m,yo,xo]+Sun_posi[line,2+m]
                        else:
                            break             
       
    return Sun_duration

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtype32_t,ndim=2] sun_shining_virt_plane_day(np.ndarray[dtype32_t,ndim=2] hole_list,
                                                              np.ndarray[dtypef_t,ndim=2] vertices,
                                                              np.ndarray[dtypef32_t,ndim=2] Dtm, 
                                                              np.ndarray[dtypef32_t,ndim=2] Sun_posi,                                                              
                                                              double dim1, double dim2,float Stand_H,
                                                              double Csize):
    
    cdef int nbhole = hole_list.shape[0]
    cdef int nbposi = Sun_posi.shape[0]
    cdef int h,i,line,test,yo,xo,j,m
    cdef double yf,xf,y3,x3,y4,x4,x,y,sl,dist,pol  
    cdef float zo,z,az
    cdef int nbsegment = vertices.shape[0]-1
    cdef double radius = (sqrt(dim1*dim1+dim2*dim2)+double_max(dim1,dim2))/Csize
    cdef int nrows = Dtm.shape[0]
    cdef int ncols =  Dtm.shape[1]
    cdef np.ndarray[dtype8_t,ndim=3] Sun_duration = np.ones((nbposi,nrows,ncols),dtype=np.int8)*-1
    cdef np.ndarray[dtypef32_t,ndim=2] Dtm2 = Dtm+Stand_H
    cdef np.ndarray[dtypef_t,ndim=2] segments = get_az_vertices(vertices)
    
    
    #loop on hole pix:
    for h in range(0,nbhole):
        yo,xo=hole_list[h,0],hole_list[h,1]
        zo= Dtm[yo,xo] 
        for m in range(0,nbposi):
            Sun_duration[m,yo,xo]=0 
        
        #calculation of azimuth of each segment
        for i in range(0,nbsegment):
            segments[i,4]=calculate_azimut(xo,-yo,segments[i,0],-segments[i,1])
            segments[i,5]=calculate_azimut(xo,-yo,segments[i,2],-segments[i,3])
            if segments[i,4]>segments[i,5]:
                segments[i,6]=1
            else:
                segments[i,6]=0
        
        #test of each azimut
        for j in range(0,nbposi):           
            az = Sun_posi[j,0]
            pol = conv_az_to_polar(az)*pi/180            
            xf = xo+radius*cos(pol)
            yf = yo-radius*sin(pol)
            line = selec_seg(az,segments)
            x3,y3,x4,y4=segments[line,0],segments[line,1],segments[line,2],segments[line,3]
            test,x,y = intersection_2segments(xo, yo, xf, yf,
                                              x3, y3, x4, y4)
            if test:                
                dist = Distplan(xo,yo,x,y)*Csize+0.00000000001
                z = get_elev(Dtm2,x,y) 
                sl = (z-zo)/dist
                if Sun_posi[j,1]>sl:                 
                    Sun_duration[j,yo,xo]=1                               
       
    return Sun_duration

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtypeu16_t,ndim=2] calc_surface_plage(np.ndarray[dtypeu16_t,ndim=2] Sun_duration,
                                                       np.ndarray[dtype32_t,ndim=1] borne_sup):
    cdef int duree_max = Sun_duration.shape[1]
    cdef int nbsimu = Sun_duration.shape[0]
    cdef int nbborne = borne_sup.shape[0]
    cdef np.ndarray[dtypeu16_t,ndim=2] Surf = np.zeros((nbsimu,nbborne+2),dtype=np.uint16)
    cdef int i,j,surf,k
    cdef int idmax,idmin
    cdef float surftot
    for i in range(nbsimu):
        idmin=1
        surftot = 0
        for k in range(duree_max):
            surftot+=Sun_duration[i,k]               
        #Surface with 0 sunduration
        Surf[i,0]=int(Sun_duration[i,0]/surftot*1000.+0.5)       
        for j in range(1,nbborne+1):
            idmax = borne_sup[j-1]
            surf = 0
            for k in range(idmin,idmax):
                surf+=Sun_duration[i,k]        
            Surf[i,j]=int(surf/surftot*1000.+0.5)
            idmin=idmax
        j+=1
        surf = 0
        for k in range(idmin,duree_max):
            surf+=Sun_duration[i,k]         
        Surf[i,j]=int(surf/surftot*1000.+0.5)
         
    return Surf

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtypeu16_t,ndim=2] calc_surface_plage2(np.ndarray[dtypeu16_t,ndim=2] Sun_duration,
                                                       np.ndarray[dtype32_t,ndim=1] borne_sup):
    cdef int duree_max = Sun_duration.shape[1]
    cdef int nbsimu = Sun_duration.shape[0]
    cdef int nbborne = borne_sup.shape[0]
    cdef np.ndarray[dtypeu16_t,ndim=2] Surf = np.zeros((nbsimu,2*nbborne+2),dtype=np.uint16)
    cdef int i,j,surf,k,col
    cdef int idmax,idmin,summ
    cdef float surftot
    for i in range(nbsimu):
        idmin=1
        surftot = 0
        for k in range(duree_max):
            surftot+=Sun_duration[i,k]               
        #Surface with 0 sunduration
        Surf[i,0]=int(Sun_duration[i,0]/surftot*1000+0.5)         
        col=1
        for j in range(1,nbborne+1):
            idmax = borne_sup[j-1]
            surf = 0
            for k in range(idmin,idmax):
                surf+=Sun_duration[i,k]        
            Surf[i,col]=int(surf/surftot*1000+0.5)
            col+=1
            Surf[i,col]=int(Sun_duration[i,idmax]/surftot*1000+0.5) 
            idmin=idmax+1
            col+=1
        j+=1
        surf = 0
        for k in range(idmin,duree_max):
            surf+=Sun_duration[i,k]         
        Surf[i,col]=int(surf/surftot*1000+0.5)
    return Surf


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtype32_t,ndim=2] sun_duration_real_terrain(np.ndarray[dtype32_t,ndim=2] hole_list,
                                                             np.ndarray[dtypef32_t,ndim=2] Mns_used,
                                                             np.ndarray[dtype32_t,ndim=2] Sun_posi,
                                                             np.ndarray[dtype32_t,ndim=2] Az_val,
                                                             np.ndarray[dtype16_t,ndim=2] Rows,
                                                             np.ndarray[dtype16_t,ndim=2] Cols,
                                                             np.ndarray[dtypef32_t,ndim=2] Dist,
                                                             np.ndarray[dtype16_t,ndim=1] Nbpix,
                                                             np.ndarray[dtype32_t,ndim=2] Sun_duration):
    cdef int nbhole = hole_list.shape[0]
    cdef int xo,yo,az,az2,signx,signy,mi,ma,npix,i,line,x,y,j
    cdef double zo,z,slmax,slmax2
    cdef int jmax = Az_val.shape[0]-1
    cdef int nbval = Sun_posi.shape[0]
    cdef int nrows=Mns_used.shape[0],ncols=Mns_used.shape[1]
    #loop on hole pix:
    for h in range(nbhole):
        yo,xo=hole_list[h,0],hole_list[h,1]
        zo=Mns_used[yo,xo]      
        Sun_duration[yo,xo]=0 

        for j in range(0,jmax+1):           
            az = Az_val[j,0]
            if az<=90:
                az2=az
                signx=1
                signy=1
            elif az>90 and az<=180:
                az2=180-az       
                signx=1
                signy=-1
            elif az>180 and az<270:
                az2=az-180
                signx=-1
                signy=-1        
            else:
                az2=360-az
                signx=-1
                signy=1   
            mi=Az_val[j,1]
            slmax=-1
            if j<jmax:
                ma = Az_val[j+1,1]
            else:
                ma = nbval  
            npix=Nbpix[az2]
            slmax2 = Sun_posi[ma-1,1]/10000.
            for i in range(1,npix):
                x = xo+signx*Cols[az2,i]
                y = yo+signy*Rows[az2,i]
                if x<0 or x>=ncols or y<0 or y>=nrows:
                    break                
                z = Mns_used[y,x] 
                if z==-9999:
                    break
                slmax = double_max(slmax,(z-zo)/Dist[az2,i]) 
                if slmax>slmax2:
                    break            
                       
            if slmax2>slmax:  
                Sun_duration[yo,xo]=Sun_duration[yo,xo]+Sun_posi[ma-1,2]
                for line in range(ma-2,mi-1,-1):
                    if Sun_posi[line,1]/10000.>slmax:
                        Sun_duration[yo,xo]=Sun_duration[yo,xo]+Sun_posi[line,2]
                    else:
                        break             
       
            #calculer les x,y en fonction de az 1 fois pour toute
               
    return Sun_duration


#Calcule la pente a partir d'un MNT et de la taille de Cellule
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtypef32_t,ndim=2] pente(np.ndarray[dtypef32_t,ndim=2] raster_mnt,double Csize,double nodata):
    """
    Calcule la pente en % a partir d'un raster MNT et d'une taille de cellule donnee
    -----
    Inputs: Raster_MNT en float, Cell size; Nodata Value
    -----
    Output : Raster de pente en %
    """
    cdef unsigned int nline= raster_mnt.shape[0]
    cdef unsigned int ncol = raster_mnt.shape[1]
    cdef np.ndarray[dtypef32_t,ndim=2] pente = np.zeros_like(raster_mnt,dtype=np.float32)
    cdef unsigned int x=1
    cdef unsigned int y=1
    cdef double a,b,c,d,e,f,g,h,i
    cdef double dz_dx,dz_dy
    # Grille sans les bordures
    for y from 1 <= y < nline-1:
        for x from 1 <= x < ncol-1:
            e = raster_mnt[y,x] 
            if e > nodata:
                a = raster_mnt[y-1,x-1]
                if a==nodata:a=e
                b = raster_mnt[y-1,x] 
                if b==nodata:b=e
                c = raster_mnt[y-1,x+1]
                if c==nodata:c=e
                d = raster_mnt[y,x-1]    
                if d==nodata:d=e
                f = raster_mnt[y,x+1]
                if f==nodata:f=e
                g = raster_mnt[y+1,x-1]  
                if g==nodata:g=e
                h = raster_mnt[y+1,x] 
                if h==nodata:h=e
                i = raster_mnt[y+1,x+1]
                if i==nodata:i=e                            
                dz_dx = float(c+2*f+i-(a+2*d+g))/float(8*Csize)
                dz_dy = float(g+2*h+i-(a+2*b+c))/float(8*Csize)
                pente[y,x]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100
            else: pente[y,x]=nodata
    # Coin superieur gauche
    if raster_mnt[0,0]>nodata:
        e = raster_mnt[0,0]
        f = raster_mnt[0,1]
        if f==nodata:f=e
        h = raster_mnt[1,0]
        if h==nodata:h=e
        i = raster_mnt[1,1]
        if i==nodata:i=e
        dz_dx = float(f+i-(e+h))/float(2*Csize)
        dz_dy = float(h+i-(d+f))/float(2*Csize)
        pente[0,0]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100      
    else: pente[0,0]=nodata
    # Coin inferieur gauche    
    if raster_mnt[nline-1,0]>nodata:
        e = raster_mnt[nline-1,0]
        b = raster_mnt[nline-2,0]
        if b==nodata:b=e
        c = raster_mnt[nline-2,1]
        if c==nodata:c=e
        f = raster_mnt[nline-1,1]
        if f==nodata:f=e
        dz_dx = float(c+f-(b+e))/float(2*Csize)
        dz_dy = float(e+f-(b+c))/float(2*Csize)
        pente[nline-1,0]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100       
    else: pente[nline-1,0]=nodata 
    # Coin superieur droite
    if raster_mnt[0,ncol-1]>nodata:
        e = raster_mnt[0,ncol-1]
        d = raster_mnt[0,ncol-2]
        if d==nodata:d=e
        g = raster_mnt[1,ncol-2]
        if g==nodata:g=e
        h = raster_mnt[1,ncol-1]
        if h==nodata:h=e
        dz_dx = float(e+h-(d+g))/float(2*Csize)
        dz_dy = float(g+h-(d+e))/float(2*Csize)
        pente[0,ncol-1]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100      
    else: pente[0,ncol-1]=nodata
    # Coin inferieur droite
    if raster_mnt[nline-1,ncol-1]>nodata:
        e = raster_mnt[nline-1,ncol-1]
        a = raster_mnt[nline-2,ncol-2]
        if a==nodata:a=e
        d = raster_mnt[nline-1,ncol-2]
        if d==nodata:d=e
        b = raster_mnt[nline-2,ncol-1]
        if b==nodata:b=e
        dz_dx = float(e+b-(d+a))/float(2*Csize)
        dz_dy = float(d+e-(a+b))/float(2*Csize)
        pente[nline-1,ncol-1]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100       
    else: pente[nline-1,ncol-1]=nodata
    # Pour premiere ligne
    x=1
    for x from 1 <= x < ncol-1:
        e = raster_mnt[0,x] 
        if e > nodata:            
            d = raster_mnt[0,x-1]    
            if d==nodata:d=e
            f = raster_mnt[0,x+1]
            if f==nodata:f=e
            g = raster_mnt[1,x-1]  
            if g==nodata:g=e
            h = raster_mnt[1,x] 
            if h==nodata:h=e
            i = raster_mnt[1,x+1]
            if i==nodata:i=e                            
            dz_dx = float(f+i-(d+g))/float(4*Csize)
            dz_dy = float(g+h+i-(d+e+f))/float(3*Csize)
            pente[0,x]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100
        else: pente[0,x]=nodata
    # Pour derniere ligne
    x=1
    for x from 1 <= x < ncol-1:
        e = raster_mnt[nline-1,x] 
        if e > nodata:            
            d = raster_mnt[nline-1,x-1]    
            if d==nodata:d=e
            f = raster_mnt[nline-1,x+1]
            if f==nodata:f=e
            a = raster_mnt[nline-2,x-1]  
            if a==nodata:a=e
            b = raster_mnt[nline-2,x] 
            if b==nodata:b=e
            c = raster_mnt[nline-2,x+1]
            if c==nodata:c=e                            
            dz_dx = float(f+c-(d+a))/float(4*Csize)
            dz_dy = float(d+e+f-(a+b+c))/float(3*Csize)
            pente[nline-1,x]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100
        else: pente[nline-1,x]=nodata
    # Pour premiere colonne
    y=1
    for y from 1 <= y < nline-1:
        e = raster_mnt[y,0] 
        if e > nodata:            
            b = raster_mnt[y+1,0]    
            if b==nodata:b=e
            c = raster_mnt[y+1,1]
            if c==nodata:c=e
            f = raster_mnt[y,1]  
            if f==nodata:f=e
            h = raster_mnt[y+1,0] 
            if h==nodata:h=e
            i = raster_mnt[y+1,1]
            if i==nodata:i=e                            
            dz_dx = float(c+f+i-(b+e+h))/float(3*Csize)
            dz_dy = float(h+i-(b+c))/float(4*Csize)
            pente[y,0]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100
        else: pente[y,0]=nodata
    # Pour derniere colonne
    y=1
    for y from 1 <= y < nline-1:
        e = raster_mnt[y,ncol-1] 
        if e > nodata:            
            a = raster_mnt[y-1,ncol-2]    
            if a==nodata:a=e
            b = raster_mnt[y-1,ncol-1]
            if b==nodata:b=e
            d = raster_mnt[y,ncol-2]  
            if d==nodata:d=e
            g = raster_mnt[y+1,ncol-2] 
            if g==nodata:g=e
            h = raster_mnt[y+1,ncol-1]
            if h==nodata:h=e                            
            dz_dx = float(b+e+h-(a+d+g))/float(3*Csize)
            dz_dy = float(h+g-(b+a))/float(4*Csize)
            pente[y,ncol-1]= sqrt(dz_dx*dz_dx+dz_dy*dz_dy)*100
        else: pente[y,ncol-1]=nodata
    return pente 

#Calcule la pente a partir d'un MNT et de la taille de Cellule
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef np.ndarray[dtypef_t,ndim=2] exposition(np.ndarray[dtypef32_t,ndim=2] raster_mnt,double Csize,double nodata):
    """
    Calcule la expo en % a partir d'un raster MNT et d'une taille de cellule donnee
    -----
    Inputs: Raster_MNT en float, Cell size; Nodata Value
    -----
    Output : Raster de expo en %
    """
    cdef unsigned int nline= raster_mnt.shape[0]
    cdef unsigned int ncol = raster_mnt.shape[1]
    cdef np.ndarray[dtypef_t,ndim=2] expo = np.zeros_like(raster_mnt,dtype=np.float)
    cdef unsigned int x=1
    cdef unsigned int y=1
    cdef double a,b,c,d,e,f,g,h,i
    cdef double dz_dx,dz_dy,expo1
    # Grille sans les bordures
    for y from 1 <= y < nline-1:
        for x from 1 <= x < ncol-1:
            e = raster_mnt[y,x] 
            if e > nodata:
                a = raster_mnt[y-1,x-1]
                if a==nodata:a=e
                b = raster_mnt[y-1,x] 
                if b==nodata:b=e
                c = raster_mnt[y-1,x+1]
                if c==nodata:c=e
                d = raster_mnt[y,x-1]    
                if d==nodata:d=e
                f = raster_mnt[y,x+1]
                if f==nodata:f=e
                g = raster_mnt[y+1,x-1]  
                if g==nodata:g=e
                h = raster_mnt[y+1,x] 
                if h==nodata:h=e
                i = raster_mnt[y+1,x+1]
                if i==nodata:i=e                            
                dz_dx = float(c+2*f+i-(a+2*d+g))/float(8*Csize)
                dz_dy = float(g+2*h+i-(a+2*b+c))/float(8*Csize)
                expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
                if expo1<0.:
                    expo[y,x]= 90.0 - expo1
                elif expo1>90.:
                    expo[y,x]= 360.0 - expo1 + 90.0
                else:
                    expo[y,x]= 90.0 - expo1
            else: expo[y,x]=nodata
    # Coin superieur gauche
    if raster_mnt[0,0]>nodata:
        e = raster_mnt[0,0]
        f = raster_mnt[0,1]
        if f==nodata:f=e
        h = raster_mnt[1,0]
        if h==nodata:h=e
        i = raster_mnt[1,1]
        if i==nodata:i=e
        dz_dx = float(f+i-(e+h))/float(2*Csize)
        dz_dy = float(h+i-(d+f))/float(2*Csize)
        expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
        if expo1<0.:
            expo[0,0]= 90.0 - expo1
        elif expo1>90.:
            expo[0,0]= 360.0 - expo1 + 90.0
        else:
            expo[0,0]= 90.0 - expo1    
    else: expo[0,0]=nodata
    # Coin inferieur gauche    
    if raster_mnt[nline-1,0]>nodata:
        e = raster_mnt[nline-1,0]
        b = raster_mnt[nline-2,0]
        if b==nodata:b=e
        c = raster_mnt[nline-2,1]
        if c==nodata:c=e
        f = raster_mnt[nline-1,1]
        if f==nodata:f=e
        dz_dx = float(c+f-(b+e))/float(2*Csize)
        dz_dy = float(e+f-(b+c))/float(2*Csize)
        expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
        if expo1<0.:
            expo[nline-1,0]= 90.0 - expo1
        elif expo1>90.:
            expo[nline-1,0]= 360.0 - expo1 + 90.0
        else:
            expo[nline-1,0]= 90.0 - expo1          
    else: expo[nline-1,0]=nodata 
    # Coin superieur droite
    if raster_mnt[0,ncol-1]>nodata:
        e = raster_mnt[0,ncol-1]
        d = raster_mnt[0,ncol-2]
        if d==nodata:d=e
        g = raster_mnt[1,ncol-2]
        if g==nodata:g=e
        h = raster_mnt[1,ncol-1]
        if h==nodata:h=e
        dz_dx = float(e+h-(d+g))/float(2*Csize)
        dz_dy = float(g+h-(d+e))/float(2*Csize)
        expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
        if expo1<0.:
            expo[0,ncol-1]= 90.0 - expo1
        elif expo1>90.:
            expo[0,ncol-1]= 360.0 - expo1 + 90.0
        else:
            expo[0,ncol-1]= 90.0 - expo1           
    else: expo[0,ncol-1]=nodata
    # Coin inferieur droite
    if raster_mnt[nline-1,ncol-1]>nodata:
        e = raster_mnt[nline-1,ncol-1]
        a = raster_mnt[nline-2,ncol-2]
        if a==nodata:a=e
        d = raster_mnt[nline-1,ncol-2]
        if d==nodata:d=e
        b = raster_mnt[nline-2,ncol-1]
        if b==nodata:b=e
        dz_dx = float(e+b-(d+a))/float(2*Csize)
        dz_dy = float(d+e-(a+b))/float(2*Csize)
        expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
        if expo1<0.:
            expo[nline-1,ncol-1]= 90.0 - expo1
        elif expo1>90.:
            expo[nline-1,ncol-1]= 360.0 - expo1 + 90.0
        else:
            expo[nline-1,ncol-1]= 90.0 - expo1 
    else: expo[nline-1,ncol-1]=nodata
    # Pour premiere ligne
    x=1
    for x from 1 <= x < ncol-1:
        e = raster_mnt[0,x] 
        if e > nodata:            
            d = raster_mnt[0,x-1]    
            if d==nodata:d=e
            f = raster_mnt[0,x+1]
            if f==nodata:f=e
            g = raster_mnt[1,x-1]  
            if g==nodata:g=e
            h = raster_mnt[1,x] 
            if h==nodata:h=e
            i = raster_mnt[1,x+1]
            if i==nodata:i=e                            
            dz_dx = float(f+i-(d+g))/float(4*Csize)
            dz_dy = float(g+h+i-(d+e+f))/float(3*Csize)
            expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
            if expo1<0.:
                expo[0,x]= 90.0 - expo1
            elif expo1>90.:
                expo[0,x]= 360.0 - expo1 + 90.0
            else:
                expo[0,x]= 90.0 - expo1 
        else: expo[0,x]=nodata
    # Pour derniere ligne
    x=1
    for x from 1 <= x < ncol-1:
        e = raster_mnt[nline-1,x] 
        if e > nodata:            
            d = raster_mnt[nline-1,x-1]    
            if d==nodata:d=e
            f = raster_mnt[nline-1,x+1]
            if f==nodata:f=e
            a = raster_mnt[nline-2,x-1]  
            if a==nodata:a=e
            b = raster_mnt[nline-2,x] 
            if b==nodata:b=e
            c = raster_mnt[nline-2,x+1]
            if c==nodata:c=e                            
            dz_dx = float(f+c-(d+a))/float(4*Csize)
            dz_dy = float(d+e+f-(a+b+c))/float(3*Csize)
            expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
            if expo1<0.:
                expo[nline-1,x]= 90.0 - expo1
            elif expo1>90.:
                expo[nline-1,x]= 360.0 - expo1 + 90.0
            else:
                expo[nline-1,x]= 90.0 - expo1 
        else: expo[nline-1,x]=nodata
    # Pour premiere colonne
    y=1
    for y from 1 <= y < nline-1:
        e = raster_mnt[y,0] 
        if e > nodata:            
            b = raster_mnt[y+1,0]    
            if b==nodata:b=e
            c = raster_mnt[y+1,1]
            if c==nodata:c=e
            f = raster_mnt[y,1]  
            if f==nodata:f=e
            h = raster_mnt[y+1,0] 
            if h==nodata:h=e
            i = raster_mnt[y+1,1]
            if i==nodata:i=e                            
            dz_dx = float(c+f+i-(b+e+h))/float(3*Csize)
            dz_dy = float(h+i-(b+c))/float(4*Csize)
            expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
            if expo1<0.:
                expo[y,0]= 90.0 - expo1
            elif expo1>90.:
                expo[y,0]= 360.0 - expo1 + 90.0
            else:
                expo[y,0]= 90.0 - expo1 
        else: expo[y,0]=nodata
    # Pour derniere colonne
    y=1
    for y from 1 <= y < nline-1:
        e = raster_mnt[y,ncol-1] 
        if e > nodata:            
            a = raster_mnt[y-1,ncol-2]    
            if a==nodata:a=e
            b = raster_mnt[y-1,ncol-1]
            if b==nodata:b=e
            d = raster_mnt[y,ncol-2]  
            if d==nodata:d=e
            g = raster_mnt[y+1,ncol-2] 
            if g==nodata:g=e
            h = raster_mnt[y+1,ncol-1]
            if h==nodata:h=e                            
            dz_dx = float(b+e+h-(a+d+g))/float(3*Csize)
            dz_dy = float(h+g-(b+a))/float(4*Csize)
            expo1 = 57.29578 * atan2(dz_dy, -dz_dx)
            if expo1<0.:
                expo[y,ncol-1]= 90.0 - expo1
            elif expo1>90.:
                expo[y,ncol-1]= 360.0 - expo1 + 90.0
            else:
                expo[y,ncol-1]= 90.0 - expo1 
        else: expo[y,ncol-1]=nodata
    return expo 


@cython.boundscheck(False)
@cython.wraparound(False)
cpdef calcul_distance_de_cout(np.ndarray[dtype8_t,ndim=2] from_rast,
                            double Csize,unsigned int Max_distance=15):    
    
    cdef unsigned int nline = from_rast.shape[0]
    cdef unsigned int ncol = from_rast.shape[1]
    cdef double diag = 1.414214*Csize
    cdef double direct = Csize
    
    # Creation des rasters de sorties
    cdef np.ndarray[dtype32_t,ndim=2] Out_distance = np.ones_like(from_rast,dtype=np.int32)*Max_distance    
    cdef unsigned int x,y,x1,y1,test,count_sans_match = 0
    cdef double Dist,dist_ac = Csize
    # Initialisation du raster
    for y1 in range(0,nline):
        for x1 in range(0,ncol):
            if from_rast[y1,x1]>0:
                Out_distance[y1,x1] = 0                
                for y in range(int_max(0,y1-1),int_min(nline,y1+2),1):
                    for x in range(int_max(0,x1-1),int_min(ncol,x1+2),1):  
                        if y!=y1 and x!=x1: Dist = diag
                        else: Dist = direct
                        if Out_distance[y,x]>Dist:
                            Out_distance[y,x] = int(Dist+0.5)                                
                                
    # Traitement complet     
    while dist_ac<=Max_distance and count_sans_match <15*Csize:
        test = 0
        for y1 in range(0,nline):
            for x1 in range(0,ncol):
                if Out_distance[y1,x1]==dist_ac:
                    test=1   
                    for y in range(int_max(0,y1-1),int_min(nline,y1+2),1):
                        for x in range(int_max(0,x1-1),int_min(ncol,x1+2),1):                            
                            if y!=y1 and x!=x1: Dist = diag+dist_ac
                            else: Dist = direct+dist_ac
                            if Out_distance[y,x]>Dist:
                                Out_distance[y,x] = int(Dist+0.5)
        if test==1:count_sans_match = 0
        else:count_sans_match +=1
        dist_ac +=1
    for y in range(0,nline,1):
        for x in range(0,ncol,1):
            if Out_distance[y,x]==Max_distance:
                Out_distance[y,x]=-9999                
    return Out_distance



# @cython.cdivision(True)
# @cython.boundscheck(False)
# @cython.wraparound(False)
# cdef int get_shape(np.ndarray[dtypeu8_t,ndim=2] Shape,
#                    double x,double y):    
#     #check if x,y is integer
#     cdef int xi = cint(x)
#     cdef int yi = cint(y)
#     cdef int xok=0
#     cdef int yok=0
#     cdef double sd,d
#     cdef int sha=Shape[yi,xi]
#     if xi==x:
#         xok = 1
#     if yi==y:
#         yok = 1    
#     # config 2 : Xint Y
#     if xok and not yok:        
#         sha = int_max(Shape[yi+1,xi],sha)       
#     # config 2 : X Yint
#     elif not xok and yok:         sha = int_max(Shape[yi,xi+1],sha)
       
#     else:        
#         sha = int_max(Shape[yi,xi+1],sha)
#         sha = int_max(Shape[yi+1,xi+1],sha)        
#         sha = int_max(Shape[yi+1,xi],sha)  
#     return sha


# @cython.cdivision(True)
# @cython.boundscheck(False)
# @cython.wraparound(False)
# cpdef np.ndarray[dtype32_t,ndim=2] sun_duration_virt_plane21(np.ndarray[dtype32_t,ndim=2] hole_list,
#                                                              np.ndarray[dtypef32_t,ndim=2] Mns, 
#                                                              np.ndarray[dtypeu8_t,ndim=2] Shape, 
#                                                              np.ndarray[dtype32_t,ndim=2] Sun_posi,
#                                                              np.ndarray[dtype32_t,ndim=2] Az_val):
#     cdef int nbhole =hole_list.shape[0]
#     cdef int xo,yo,i,j,mi,ma,h,inshape,sha,line,m
#     cdef double zo,z,x,y,slmax,sina,cosa,az,pol
#     cdef int jmax = Az_val.shape[0]-1
#     cdef int nbval = Sun_posi.shape[0]
#     cdef int nrows = Mns.shape[0]
#     cdef int ncols = Mns.shape[1]
#     cdef np.ndarray[dtype32_t,ndim=3] Sun_duration = np.ones((12,nrows,ncols),dtype=np.int32)*-1
#     #loop on hole pix:
#     for h in range(0,nbhole):
#         yo,xo=hole_list[h,0],hole_list[h,1]
#         zo=Mns[yo,xo] 
#         for m in range(0,12):
#             Sun_duration[m,yo,xo]=0 

#         for j in range(0,jmax+1):           
#             az = Az_val[j,0]
#             mi=Az_val[j,1]
#             slmax=-1
#             if j<jmax:
#                 ma = Az_val[j+1,1]
#             else:
#                 ma = nbval  
#             pol = conv_az_to_polar(az)*pi/180
#             cosa = cos(pol)
#             sina = sin(pol)
#             i=1
#             inshape = 1
#             nbmin=0
            
#             while inshape:
#                 x = xo+i*cosa
#                 y = yo-i*sina
#                 z,sha = get_elev_shape(Mns,Shape,x,y) 
#                 # if (z-zo)<0:
#                 #     slmax=10000
#                 #     break
#                 slmax = double_max(slmax,(z-zo)/i) 
#                 if not sha:
#                     inshape=0
#                     break
#                 i+=1
            
#             if Sun_posi[ma-1,1]/10000.>slmax:    
#                 azOK=1
#                 for m in range(0,12):    
#                     Sun_duration[m,yo,xo]=Sun_duration[m,yo,xo]+Sun_posi[ma-1,2+m]
#                 for line in range(ma-2,mi-1,-1):
#                     if Sun_posi[line,1]/10000.>slmax:
#                         for m in range(0,12):   
#                             Sun_duration[m,yo,xo]=Sun_duration[m,yo,xo]+Sun_posi[line,2+m]
#                     else:
#                         break             
       
#     return Sun_duration




# @cython.cdivision(True)
# @cython.boundscheck(False)
# @cython.wraparound(False)
# cpdef sun_dur_rad_virt_plane(np.ndarray[dtype32_t,ndim=2] hole_list,                            
#                              np.ndarray[dtypef32_t,ndim=2] Mns, 
#                              np.ndarray[dtypeu8_t,ndim=2] Shape, 
#                              np.ndarray[dtypef32_t,ndim=2] Sun_posi,
#                              np.ndarray[dtype32_t,ndim=2] Az_val,
#                              double Rmin):
    
#     cdef int nbhole =hole_list.shape[0]
#     cdef int xo,yo,i,j,mi,ma,h,inshape,sha,line
#     cdef double zo,z,x,y,slmax,sina,cosa,az,pol
#     cdef int jmax = Az_val.shape[0]-1
#     cdef int nbval = Sun_posi.shape[0]
#     cdef np.ndarray[dtype32_t,ndim=2] Sun_duration = np.ones_like(Mns,dtype=np.int32)*-1
#     cdef np.ndarray[dtypef32_t,ndim=2] Sun_rad_dir = np.ones_like(Mns,dtype=np.float32)*-1
#     #loop on hole pix:
#     for h from 0 <= h < nbhole:
#         yo,xo=hole_list[h,0],hole_list[h,1]
#         zo=Mns[yo,xo]      
#         Sun_duration[yo,xo]=0 
#         Sun_rad_dir[yo,xo]=Rmin  
        
#         for j from 0 <= j < jmax+1:              
#             az = Az_val[j,0]
#             mi=Az_val[j,1]
#             slmax=-1
#             if j<jmax:
#                 ma = Az_val[j+1,1]
#             else:
#                 ma = nbval  
#             pol = conv_az_to_polar(az)*pi/180
#             cosa = cos(pol)
#             sina = sin(pol)
#             i=1
#             inshape = 1
#             nbmin=0
            
#             while inshape:
#                 x = xo+i*cosa
#                 y = yo-i*sina
#                 z,sha = get_elev_shape(Mns,Shape,x,y) 
#                 # if (z-zo)<0:
#                 #     slmax=10000
#                 #     break
#                 slmax = double_max(slmax,(z-zo)/i) 
#                 if not sha:
#                     inshape=0
#                     break
#                 i+=1
            
#             if Sun_posi[ma-1,1]>slmax:    
#                 azOK=1
#                 Sun_duration[yo,xo]=Sun_duration[yo,xo]+int(Sun_posi[ma-1,2])
#                 Sun_rad_dir[yo,xo]=Sun_rad_dir[yo,xo]+Sun_posi[ma-1,3]
#                 for line in range(ma-2,mi-1,-1):
#                     if Sun_posi[line,1]>slmax:
#                         Sun_duration[yo,xo]=Sun_duration[yo,xo]+int(Sun_posi[line,2])
#                         Sun_rad_dir[yo,xo]=Sun_rad_dir[yo,xo]+Sun_posi[line,3]
#                     else:
#                         break      
        
#         Sun_rad_dir[yo,xo]=Sun_rad_dir[yo,xo]/1000000. #Month value in MJ/m²
#     return Sun_duration,Sun_rad_dir