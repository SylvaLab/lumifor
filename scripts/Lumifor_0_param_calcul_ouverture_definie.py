# -*- coding: utf8 -*-
"""
Software: LUMIFOR
File: Lumifor_0_param_calcul_ouverture_definie.py
Copyright (C) Sylvain DUPIRE 2023
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 1.1
Date: 2023/04/27
License :  GNU-GPL V3
"""

import sys,traceback,os,shutil,time
from Lumifor_1_function import *
from distutils.dir_util import copy_tree


###############################################################################
### Parametres
###############################################################################
SC_Rspace = "F:/SylvaLab/Projets/ONF/Lumifor/Scripts/Results/" #Dossier de résultats

############################
### Position
############################
Lat = 45.4290     # °    Latitude
Lon = 5.979187    # °    Longitude
Elevation = 343   # m    Altitude

###########################
### Parametre du versant
############################
Dtm_sl = 10                # %          Pente du versant
Dtm_az_gd = 0              # grades     Exposition du versant

############################
### Parametre du peuplement
############################
Stand_H = 24               # m          Hauteur du peuplement

############################
### Parametre de la trouee
############################
form = "ellipse"            # Forme de la trouée : 'rectangle' ou 'ellipse'
dim1 = 1.5                  # Dimension dans la 1ere dimension de la trouée en
                            # fonction de la hauteur de peuplement
dim2 = 1.25                 # Dimension dans la 2eme dimension de la trouée en
                            # fonction de la hauteur de peuplement
f_az_gd = 150               # grades    Direction de la plus grande dimension    
                            # par rappord à l'axe de plus grande pente

############################
### Parametres de calculs
############################
calc_june = True    # Calculer l'ensoleillement direct moyen en juin (True ou False)
calc_month = True   # Calculer l'ensoleillement direct le 21 de chaque mois (True ou False)
calc_day = True     # Calculer la surface au soleil au cours de la journée du 21juin (True ou False)
Tlaps = 1           # min  Pas d'analyse 
Csize = 1           # m    Resolution du pixel
borne_sup = [1,2,4,6]
###############################################################################
### Process
###############################################################################
DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,DB_dim1,DB_dim2, \
      DB_f_az,DB_Lat,DB_Lpente,DB_Surf,Radiations,Clouds=load_Database()    
      
tree = spatial.cKDTree(Clouds[:,:2]/100.)      
Dtm_az = (Dtm_az_gd*18/20)%360
f_az = (f_az_gd*18/20+Dtm_az)%360 

Rspace_B,June_ok,Month_ok,Day_ok,Rad_ok = create_res_dir(SC_Rspace,Dtm_az,
                                                         Dtm_sl,Elevation,
                                                         Stand_H,form,
                                                         dim1,dim2,
                                                         f_az_gd,Lat,Lon)
         
try:
    if not Rad_ok:
        plot_rad_scale_v(Radiations,Lat,Lon,Elevation,Dtm_az*20/18,Dtm_sl,
                         Clouds,tree,Rspace_B+"Rayonnement.png",col=3)
    
    if min(June_ok,Month_ok,Day_ok)==False:     
        Temp_Rspace = SC_Rspace + "Temp/"
        Dtm,hole_list,Shape,vertices = build_plane_shape(Elevation,Csize,
                                                         Dtm_sl,Dtm_az, 
                                                         Stand_H,form,
                                                         dim1,dim2,f_az,
                                                         Temp_Rspace)
    if calc_june and not June_ok:
        Sd,nbdays = Month_Av_Daily_sun_duration(Lat,Lon,Elevation,
                                                Tlaps,Csize,
                                                Dtm_sl,Dtm_az,
                                                Stand_H,form,
                                                dim1,dim2,f_az,
                                                Rspace_B,Dtm,
                                                hole_list,Shape,
                                                vertices,Month=6)                
                       
        complete_resfile(SC_Rspace,Dtm_az,Dtm_sl,Elevation,Stand_H,
                         form,dim1,dim2,f_az_gd,Lat,Lon,borne_sup,
                         Sd,nbdays,Shape)
        
    if calc_month and not Month_ok:
        Month_21_sun_duration(Lat,Lon,Elevation,Tlaps,Csize,Dtm_sl,Dtm_az,
                              Stand_H,form,dim1,dim2,f_az,Rspace_B,
                              Dtm,hole_list,Shape,vertices)
    if calc_day and not Day_ok:
        fig_list = Day_sun_course(Lat,Lon,Elevation,Csize,Dtm_sl,Dtm_az,
                              Stand_H,form,dim1,dim2,f_az,Rspace_B,
                              Dtm,hole_list,Shape,vertices,
                              Tlaps=30,Month=6,Day=21)
    
    shutil.rmtree(Temp_Rspace)   
    print("Calcul de la durée d'ensoleillement terminé")  
    
except Exception:    
    log = open(SC_Rspace + 'error_log.txt', 'w')
    traceback.print_exc(file=log)
    log.close() 
