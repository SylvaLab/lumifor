# -*- coding: utf8 -*-
"""
Software: LUMIFOR
File: Lumifor_0_param_calcul_terrain_reel.py
Copyright (C) Sylvain DUPIRE 2023
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 1.1
Date: 2023/04/27
License :  GNU-GPL V3
"""

import sys,traceback,os,shutil,time
from Lumifor_1_function import *
from distutils.dir_util import copy_tree

###############################################################################
### Parametres
###############################################################################
Ter_Rspace = "F:/Zone_test/Chamonix/Results/" # Dossier de résultats

############################
### Fichiers géographiques
############################
MNT_file = 'F:/Zone_test/Chamonix/Mnt_1m.tif'       # MNT
MNS_file = 'F:/Zone_test/Chamonix/Mne_1m.tif'       # MNS
Trouee_file = 'F:/Zone_test/Chamonix/trouee.shp'    # Shapefile de polygones representant les ouvertures
                                            

###############################################################################
### Process
###############################################################################

Res,mess,nbtrou = process_calc_terrain(MNT_file,MNS_file,Trouee_file,
                                       Ter_Rspace,save_sig=True,save_fig=True,
                                       buff=300,borne_sup=[1,2,4,6])
             
           
print(mess) 




